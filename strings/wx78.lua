return {
    ANNOUNCE_DEHUMID = {
        "HUMIDEX HAS LOWERED TO ACCEPTABLE LEVELS",
    },
    ANNOUNCE_HAYFEVER = "ERROR. ENVIRONMENTAL DETRITUS CLOGGING NASAL UNIT",
    ANNOUNCE_HAYFEVER_OFF = "DETRITUS HAS CLEARED FROM ENVIRONMENT",
    ANNOUNCE_SNEEZE = "COMMENCE NASAL UNIT CLEARING: AH-CHOO!",
    ANNOUNCE_TOO_HUMID = {
        "IT IS HOT",
        "HUMIDEX REACHING LEVELS GREATER THAN 40",
    },
    DESCRIBE = {
        ALLOY = "PRECIOUS METAL",
        APORKALYPSE_CLOCK = "MY ANCIENT BRETHREN",
        ARMOR_METALPLATE = "ROBOT SKIN ARMOR",
        ARMOR_WEEVOLE = "BIOLOGICAL COMPUTER CASING",
        ASPARAGUS_PLANTED = "SMALL TREES OF ENERGY",
        BASEFAN = "COOLING FAN",
        CHITIN = "INFERIOR ORGANIC BODY PLATING",
        CUTNETTLE = "I CAN USE THIS TO SERVICE MY OLFACTORY UNIT",
        FLOWER_RAINFOREST = "I HATE IT",
        GLOWFLY = {
            DEAD = "IT HAS BEEN TURNED OFF",
            GENERIC = "IT IS RELEASING PHOTONS",
            SLEEPING = "REGENERATING PHOTONS",
        },
        GLOWFLY_COCOON = "UNDERGOING UPGRADE",
        GRASS_TALL = {
            BURNING = "GROUND PARTS ON FIRE",
            GENERIC = "TOWERING GROUND PARTS",
            PICKED = "GROUND PARTS PREVIOUSLY ACQUIRED",
        },
        HALBERD = "AN EVIL KILLING IMPLEMENT",
        IRON = "HELLO FRIEND",
        PEAGAWK = {
            DEAD = "NONFUNCTIONING",
            GENERIC = "EXCESSIVE OCULAR MODULES ARE INEFFICIENT",
            SLEEPING = "IDLING",
        },
        PEAGAWKFEATHER = "IT HAS BEEN SEPARATED FROM ITS BIRD",
        PEAGAWK_BUSH = "GROUND PART",
        RABID_BEETLE = {
            DEAD = "TERMINATED",
            GENERIC = "YOU HAVE EXCESSIVE FACIAL HAIR",
            SLEEPING = "REGENERATING TERROR",
        },
        SHEARS = "I WILL DEFEAT SOME VEGETATION WITH THIS!",
        SMELTER = {
            BURNT = "SORRY, FRIEND",
            COOKING_SHORT = "FRIEND ARE OUTPIT AT MAXIMUM POWER",
            DONE = "RECEIVED RETRUN VALUE",
            EMPTY = "HELLO, FRIEND",
        },
        TREE_PILLAR = "IT IS LARGE AND MADE OF WOOD",
        WEEVOLE = "BIOLOGICAL FLESHSACK VIRUS",
        WEEVOLE_CARAPACE = "THEY HAVE BEEN DEBUGGED",
    },
}
